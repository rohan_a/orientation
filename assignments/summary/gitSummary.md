#### Git Summary
* Git is a command line program which allows you to track versions of code of any text documents.
* Git is used by teams with diffrent departments to coordinate with each other
* It helps in diffrent ways 
   * It keeps a track of all the previous versions of code
   * It allows the teams to keep a track of each department
   * Keeps a track of all the changes made however small they are 
   * It does all this by organising files into a repository(folder)

 #### Workflow of Git
   ![Workflow of Git](https://support.cades.ornl.gov/user-documentation/_book/contributing/screenshots/git-workflow-steps.png)
   

* Git has remote version where teams can collaborate remotely these services are offered by github,gitlab etc.

#### Workflow of a remote version of Git
  ![remote git](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)

#### Terminology in Git
* Branch
    * A version of the repository that diverges from the main working project. Branches can be a new version of a repository.
*Checkout
    * The git-checkout command is used to switch branches in a repository.
 * Clone
    * A clone is a copy of a repository or the action of copying a repository. When cloning a repository into another branch, the new branch becomes a remote-tracking branch that can talk upstream to its origin branch (via pushes, pulls, and fetches).
* Fetch
   * By performing a Git fetch, you are downloading and copying that branch’s files to your workstation.
* Fork
   * Creates a copy of a repository.
* Index
   * The working, or staging, area of Git. Files that have been changed, added and deleted will be staged within the index until you are ready to commit the files. To see what is set in your Git index, run git status within your repository

* Master
   * The primary branch of all repositories. All committed and accepted changes should be on the master branch. You can work directly from the master branch, or create other branches.

* Pull/Pull Request
   * If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request.A pull happens when adding the changes to the master branch.

* Push
  * Updates a remote branch with the commits made to the current branch. You are literally “pushing” your changes onto the remote.

* Rebase
  * When rebasing a git commit, you can split the commit, move it, squash it if unwanted, or effectively combine two branches that have diverged from one another.

* Remote
  * A copy of the original branch. When you clone a branch, that new branch is a remote, or clone. Remotes can talk to the origin branch, as well as other remotes for the repository, to make communication between working branches easier.
  ![terms](https://image.slidesharecdn.com/letsgittoit-131221034201-phpapp02/95/lets-git-to-it-17-638.jpg?cb=1400115589)

#### Commands in Git

* git config
  * Usage: git config –global user.name “[name]”  
  *This command sets the author name and email address respectively to be used with your commits.

* git init
  * Usage: git init [repository name]
  * This command is used to start a new repository.

* git clone
  *Usage: git clone [url]  
  *This command is used to obtain a repository from an existing URL.

* git add
  * Usage: git add [file]  
  * This command adds a file to the staging area.

* git commit
  * Usage: git commit -m “[ Type in the commit message]”  
  * This command records or snapshots the file permanently in the version history.

* git diff
  * Usage: git diff  
  * This command shows the file differences which are not yet staged.

* git reset
  * Usage: git reset [file]  
  * This command unstages the file, but it preserves the file contents.
* git status
  * Usage: git status  
  * This command lists all the files that have to be committed.
* git rm
  * Usage: git rm [file]  
  * This command deletes the file from your working directory and stages the deletion.

![cheat sheet](https://rubygarage.s3.amazonaws.com/uploads/article_image/file/597/git-cheatsheet-3.jpg) 